import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def assign_code(name):
    if 'Mr.' in name:
        return 1
    elif 'Mrs.' in name:
        return 2
    elif 'Miss.' in name:
        return 3
    return 0


def get_filled():
    df = get_titatic_dataframe()
    df = df[['Name', 'Age']]
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    to_found = ["Mr.", "Mrs.", "Miss."]
    
    
    df['code'] = df['Name'].apply(assign_code)
    
    mean_ages = df.groupby('code')['Age'].mean()
    mean_ages = mean_ages.to_dict()
    
    nans_by_category = df.groupby('code')['Age'].apply(lambda x: x.isna().sum())
    nans_by_category = nans_by_category.to_dict()
    res = []
    for entity in to_found:
        code = assign_code(entity)
        res_tuple = (entity, nans_by_category[code], int(mean_ages[code]))
        res.append(res_tuple)
    return res

